<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use illuminate\database\Eloquent\casts\Attribute;

class transactions extends Model
{
    use HasFactory;
    /**
     * fillable
     * @var array
     */
    protected $fillable = [
        'cashier_id', 'customer_id', 'invoice', 'cash', 'change', 'discount', 'grand_total', 'table_total'
    ];

    /**
     * details
     * 
     * @return void
     */
    public function details()
    {
        return $this->hasMany(transaction_details::class);
    }

    /**
     * customers
     * 
     * @return void
     */
    public function costumer()
    {
        return $this->belongsTo(customers::class);
    }

    /**
     * cashier
     * 
     * @return void
     */
    public function cashier()
    {
        return $this->belongsTo(user::class, 'cashier_id');
    }

    /**
     * profit
     * 
     * @return void
     */
    public function profit()
    {
        return $this->hasMany(products::class);
    }

    /**
     * craeteAd
     * 
     * @return attribute
     */
    protected function createAd(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon::parse(($value)->date_format)
        );
    }
}