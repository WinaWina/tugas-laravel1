<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class customers extends Model
{
    use HasFactory;
    /**
     * fillable
     * 
     * @var array
     */
    protected $fillable = [
        'name', 'no_telp', 'address'
    ];
    /**
     * tracsactions
     * 
     * @return void
     */
    public function transactions()
    {
        return $this->hasMany(transactions::class);
    }
}