<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use illuminate\database\Eloquent\casts\Attribute;

class products extends Model
{
    use HasFactory;
    /**
     * fillable
     * 
     * @var array
     */
    protected $fillable = [
        'image', 'barcode', 'title', 'description', 'buy_price', 'cell_price', 'stock'
    ];

    /**
     * categories
     * 
     * @return void
     */
    public function categories()
    {
        return $this->belongsTo(categories::class);
    }

    /**
     * details
     * 
     * @return void
     */
    public function details()
    {
        return $this->hasMany(transactions_details::class);
    }
    /**
     * image
     * 
     * @return attribute
     */

    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => asset('/storage/products/' . $value),
        );
    }


}